import { useForm } from "react-hook-form"
import { translationAdd } from "../../Api/translation"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../context/UserContext"
import { storageSave } from "../../Utils/storage"

const userKey = STORAGE_KEY_USER

// Used for configuring error check on user input.
const inputConfig = {
  required: true,
  maxLength: 40,
  pattern: /[a-zA-Z\s]$/,
}

const TranslationActions = ({ setInput }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const { user, setUser } = useUser()

  // Sends user input to parent component. Also adds translation to user history
  const handleInput = async ({ inputToTranslate }) => {
    setInput(inputToTranslate)
    const [error, updatedUser] = await translationAdd(user, inputToTranslate)
    if (error !== null) {
      console.log(error)
      return
    }

    storageSave(userKey, updatedUser)
    setUser(updatedUser)
  }

  const errorMsg = (() => {
    if (!errors.inputToTranslate) {
      return null
    }

    if (errors.inputToTranslate.type === "required") {
      return <span className="error-msg">Translation Text is Required</span>
    }

    if (errors.inputToTranslate.type === "maxLength") {
      return <span className="error-msg">Translation is too Long</span>
    }

    if (errors.inputToTranslate.type === "pattern") {
      return <span className="error-msg">You Can Only Translate Letters</span>
    }
  })()

  return (
    <div>
      <div className="input">
        <form onSubmit={handleSubmit(handleInput)}>
          <input
            type="text"
            autoComplete="off"
            placeholder="Hello!"
            {...register("inputToTranslate", inputConfig)}
          />
          <button className="btn" type="submit">
            Translate
          </button>
        </form>
      </div>
      {errorMsg}
    </div>
  )
}
export default TranslationActions
