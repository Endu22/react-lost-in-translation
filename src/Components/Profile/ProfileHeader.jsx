const ProfileHeader = ({ username }) => {
  return (
    <header>
      <img
        id="profile-page-avatar"
        src="images/profile-avatar.png"
        alt="Profile Avatar"
      ></img>
      <h4>Hello {username}, nice to see you!</h4>
    </header>
  )
}
export default ProfileHeader
