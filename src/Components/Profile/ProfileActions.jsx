import { Link } from "react-router-dom"
import { storageDelete, storageSave } from "../../Utils/storage"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { translationClearHistory } from "../../Api/translation"
import { useUser } from "../../context/UserContext"

const ProfileActions = () => {
  const { user, setUser } = useUser()

  // Logs out user, removes user from sessionStorage
  const handleLogoutClick = () => {
    if (window.confirm("Are you sure you want to logout?")) {
      storageDelete(STORAGE_KEY_USER)
      setUser(null)
    }
  }

  // Updates translation history to 'empty' from sessionStorage and API.
  const handleClearHistoryClick = async () => {
    if (
      !window.confirm(
        "Are you sure you want to delete translation history?\nTHIS CAN NOT BE UNDONE!"
      )
    ) {
      return
    }

    const [clearError] = await translationClearHistory(user.id)

    if (clearError !== null) return

    const updatedUser = {
      ...user,
      translations: [],
    }

    storageSave(STORAGE_KEY_USER, updatedUser)
    setUser(updatedUser)
  }

  return (
    <ul id="profile-actions">
      <li>
        <Link to="/translate">
          <button className="btn">Translate</button>
        </Link>
      </li>
      <li>
        <button className="btn" onClick={handleClearHistoryClick}>
          Clear History
        </button>
      </li>
      <li id="logout-item">
        <button className="btn" onClick={handleLogoutClick}>
          Log Out
        </button>
      </li>
    </ul>
  )
}
export default ProfileActions
