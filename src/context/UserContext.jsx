import { createContext, useContext, useState } from "react"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { storageRead } from "../Utils/storage"

const UserContext = createContext()

export const useUser = () => {
  return useContext(UserContext) // return [ user, setUser ]
}

// Gets user from sessionStorage on first render. Passes it down to children.
const UserProvider = ({ children }) => {
  const [user, setUser] = useState(storageRead(STORAGE_KEY_USER))

  const state = {
    user,
    setUser,
  }

  return <UserContext.Provider value={state}>{children}</UserContext.Provider>
}
export default UserProvider
