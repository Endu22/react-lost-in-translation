// NOTE: Used sessionStorage instead of localStorage
// to prevent data mismatch between app and API. (API resets every 30 min).

export const storageSave = (key, value) => {
  sessionStorage.setItem(key, JSON.stringify(value))
}

export const storageRead = (key) => {
  const data = sessionStorage.getItem(key)
  if (data) {
    return JSON.parse(data)
  }

  return null
}

export const storageDelete = (key) => {
  if (!key) throw new Error("storageDelete: No key Provided")

  sessionStorage.removeItem(key)
}
