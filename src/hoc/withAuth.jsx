import { Navigate } from "react-router-dom"
import { useUser } from "../context/UserContext"

// Checks if user is logged in. If not logged in, redirects to login page.
// Else continue with navigation
const withAuth = (Component) => (props) => {
  const { user } = useUser()

  if (user !== null) {
    return <Component {...props} />
  } else {
    return <Navigate to="/" />
  }
}
export default withAuth
