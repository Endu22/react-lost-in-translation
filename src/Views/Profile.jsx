import ProfileActions from "../Components/Profile/ProfileActions"
import ProfileHeader from "../Components/Profile/ProfileHeader"
import ProfileTranslationsHistory from "../Components/Profile/ProfileTranslationsHistory"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"

const Profile = () => {
  const { user } = useUser()

  return (
    <div>
      <div id="profile-window">
        <ProfileHeader username={user.username} />
        <div id="profile-content">
          <ProfileTranslationsHistory translations={user.translations} />
          <ProfileActions />
        </div>
      </div>
      <div id="footer-area"></div>
    </div>
  )
}
export default withAuth(Profile)
