import React from "react"
import "./App.css"
import Login from "./Views/Login"
import Translator from "./Views/Translator"
import Profile from "./Views/Profile"
import Navbar from "./Components/Navbar/Navbar"

import { BrowserRouter, Routes, Route } from "react-router-dom"

function App() {
  return (
    <BrowserRouter>
      <div id="app-background">
        <Navbar />
        <Routes>
          <Route path="/" element={<Login />} />
          <Route path="/translate" element={<Translator />} />
          <Route path="/profile" element={<Profile />} />
        </Routes>
      </div>
    </BrowserRouter>
  )
}

export default App
